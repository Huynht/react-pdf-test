import logo from './logo.svg';
import './App.css';
import {useState} from 'react'
import { Document, Page } from 'react-pdf'
import sample from './sample.pdf'

function App() {
  const [pageNumber] = useState(1)

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Document file="./sample.pdf" onLoadError={(e) => console.log(e)}>
          <Page pageNumber={pageNumber} />
        </Document>
        <Document file="/home/an/Dropbox/Studium/aktuell/MA/example_base_html/UNR46/4.1.0/proposed/ECE-TRANS-WP29-2014-009e.pdf" onLoadError={(e) => console.log(e)}>
          <Page pageNumber={pageNumber} />
        </Document>
        <Document file={sample} onLoadError={(e) => console.log(e)}>
          <Page pageNumber={pageNumber} />
        </Document>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
